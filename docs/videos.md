# How to videos erstellen
Um euch die Vorstellung eurer Hackathon Ergebnisse zu erleichtern, möchten wir euch hier einige kostenlose Tools auflisten, mit denen
ihr kurze Videos machen könnt.

## Linux
- Audioaufnahme + Schnitt: Audacity
- Bildschirmaufnahme: SimpleScreenCast oder OBS Studio
- Bildbearbeitung: GIMP
- Videoschnitt: Kdenlive oder Shotcut

## MacOS
- Audioaufnahme + Schnitt: Audacity oder QuickTime
- Bildschirmaufnahme: OBS Studio oder QuickTime Player
- Bildbearbeitung: GIMP
- Videoschnitt: Shotcut oder Apple iMovie

## Windows
- Audioaufnahme + Schnitt: Audacity
- Bildschirmaufnahme: OBS Studio
- Bildbearbeitung: GIMP
- Videoschnitt: Shotcut oder Microsoft Photos
