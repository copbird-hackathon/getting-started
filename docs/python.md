# Python

## Installation
- [Anleitung für Windows](https://installpython3.com/windows/)
- [Anleitung für Linux](https://installpython3.com/linux/)
- [Anleitung für macos](https://installpython3.com/mac/)

Alternativ kann Python für Windows auch mit Anaconda installiert werden: https://docs.anaconda.com/anaconda/install/windows/
> "With over 25 million users worldwide, the open-source Individual Edition (Distribution) is the easiest way to perform Python/R data science and machine learning on a single machine. Developed for solo practitioners, it is the toolkit that equips you to work with thousands of open-source packages and libraries."

Mit Anaconda kannst du auch R installieren. Außerdem bietet Anaconda als Paketmanager Zugriff auf die wichtigsten Tools zur Datenverarbeitung in Python und R.

## Tutorials
- Python Einführung (deutsch): https://www.w3schools.com/python/default.asp
- Python Einführung (englisch): https://python.swaroopch.com
- Python Sprachanalyse/ NLP Einführung: https://www.tutorialspoint.com/natural_language_processing/index.htm
- Python NLTK Sentiment Analysis: https://www.digitalocean.com/community/tutorials/how-to-perform-sentiment-analysis-in-python-3-using-the-natural-language-toolkit-nltk

## Bibliotheken
Python-Bibliotheken oder Python-Module liefern eine Menge Funktionen und können euch viel Arbeit abnehmen. 
So gibt es z.B. Bibliotheken die Daten für euch analysieren oder auch grafisch darstellen. 
Eine kleine Auswahl an Empfehlungen seht ihr hier:

- NumPy (https://numpy.org/) - "scientific computing with Python" (gut für Verarbeitung von numerischen Werten)  
- Pandas (https://pandas.pydata.org/) - "data analysis and manipulation tool" (gut für Daten in tabellarischer Form)  
- Matplotlib (https://matplotlib.org/) - "Visualization with Python" (ermöglicht die vielfältige visuelle Darstellung von Daten)  
- spaCy (https://spacy.io/) - "Industrial Strength Natural Language Processing" (starkes Textanalysetool Tokenizing, Stemming, etc.)  
- NLTK (https://www.nltk.org/) - "Natural Language Toolkit" (ähnlich zu spaCy)  
- scikit-learn (https://scikit-learn.org/stable/) - "Machine Learning in Python" (sehr umfangreiche Bibliothek mit Fokus auf machine learning Algorithmen, kann z.B. auch zur sentiment analysis verwendet werden, siehe auch [scikit-learn-Tutorial](https://kgptalkie.com/sentiment-analysis-using-scikit-learn/))


