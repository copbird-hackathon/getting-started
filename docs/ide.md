# Entwicklungsumgebungen

## PyCharm
- [PyCharm](https://www.jetbrains.com/pycharm/) ist eine professionelle Entwicklungsumgebung (IDE) für Python mit vielen Features
- In der "Community" Edition is PyCharm auch kostenlos für alle Nutzer:innen
- Nutzer:innen, die an einer Bildungseinrichtung eingeschrieben sind, können die PyCharm Professional Lizenz auch kostenlos erwerben. Dazu wird ein JetBrains-Konto benötigt. 
  
## VSCodium
- [VSCodium](https://vscodium.com/) ist ein erweiterbarer Texteditor, der sich durch viele Plugins zu einer IDE ausbauen lässt.
- Das Programm ist kostenfrei und Open Source.
- Hinweis: VSCodium ist im Endeffekt [VSCode](https://code.visualstudio.com/), nur ohne Microsoft-Überwachungsmist.

## Jupyter Notebooks
- Mit Jupyter Notebooks können Arbeitsabläufe in Python und anderen Programmiersprachen gleichzeitig dokumentiert und ausgeführt werden, wie in einem Essay, das auch Code-Schnipsel beinhaltet. Ihr könnt Jupyter Notebooks lokal ausführen (wie unsere Beispiel-Notebooks).
- Online-Plattformen wie Google Colab oder Deepnotes bieten die Möglichkeit, Jupyter Notebooks online und in Echtzeit-Kollaboration auszuführen. 
  Praktisch: Ihr müsst euch dafür lokal kein Python installieren. Hier sind einige Anbieter:
    - [Google Colab](https://colab.research.google.com/)
    - [Kaggle Kernels](https://www.kaggle.com/kernels)
    - [Deepnote](https://deepnote.com/)
    - [Binder](https://mybinder.org/)
    - [Curvenote](https://curvenote.com/)
    
## RStudio
- [RStudio](https://www.rstudio.com/products/rstudio/) ist eine IDE für die Programmiersprache R inklusive Plot-Anzeige, Debugger, Autocomplete usw.
- Die Funktionalität kann mit Paketen aus dem [CRAN-Repository](https://cran.r-project.org/) erweitert werden.