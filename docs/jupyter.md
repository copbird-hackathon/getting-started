# Jupyter Notebook

## Lokal
1. Überprüfen, ob Python3 installiert ist.
```
python3 --version
```
Falls nicht, Python installieren:
    a) [Anleitung für Windows](https://installpython3.com/windows/)
    b) [Anleitung für Linux](https://installpython3.com/linux/)
    c) [Anleitung für macos](https://installpython3.com/mac/)

2. Überprüfen, ob `python3-pip` installiert ist.
```
pip3 --version
```
Falls nicht, Pip installieren: [Anleitung](https://pip.pypa.io/en/stable/installing/)

3. `virtualenv` installieren.
```
pip3 install virtualenv
```

4. In den Projektordner wechseln und eine neue virtuelle Umgebung erstellen:
```
virtualenv copbird-venv
```
5. In die virtuelle Umgebung wechseln.
```
source copbird-venv/bin/activate
```
6. Abhängigkeiten installieren.
```
pip install -r requirements.txt
```
7. Die neueste Version von `snscrape` installieren.
```
pip install git+https://github.com/JustAnotherArchivist/snscrape.git
```
8. CSV-Dateien im Ordner `data` ablegen.
9. Jupyter Notebook starten:
```
jupyter notebook
```

## Online
- [Google Colab](https://colab.research.google.com/)
- [Kaggle Kernels](https://www.kaggle.com/kernels)
- [Deepnote](https://deepnote.com/)
- [Binder](https://mybinder.org/)
- [Curvenote](https://curvenote.com/)