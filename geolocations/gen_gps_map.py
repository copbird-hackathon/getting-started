import pandas as pd
import folium

# create a base map with a view to Germany
map_ = folium.Map(location=[51.423, 9.03], zoom_start=6)

# read in the CSV file containing the LAT, LONG values
# CSV headers are included since we set index_col to 0
df = pd.read_csv("polizei_accounts_geo.csv", delimiter="\t", index_col=0)

# skip first two rows since they do not contain GPS data
df = df.iloc[2:, :]

# create a new dataframe from the LAT and LONG columns
locs = df[["LAT", "LONG"]]
# create list of tuples
loc_list = locs.values.tolist()

for point in range(0, len(loc_list)):
    folium.Marker(loc_list[point]).add_to(map_)

# save map to file called "index.html" -> can be shown with browsers
map_.save("index.html")
