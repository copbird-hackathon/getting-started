# Geopositionen der Twitter-Accounts

## Tabelle
Die erste Reihe besteht aus den Namen der Header (Spaltennamen).

Es gibt folgende Spalten, welche mit `\t` (Tabulator) voneinander getrennt sind:
- `Polizei Account` - Twitter Account-Name (kleingeschrieben/ lowered)
- `Name` - Beschreibungsname des Accounts
- `Typ` - Typ der polizeilichen Institution (LKA, Polizei, Bundespolizei, ...)
- `Bundesland` - Bundesland für welches der Twitter Account gilt, falls ein Twitter Account für mehrere Bundesländer zuständig ist wurde trotzdem nur eines dieserBundesländer eingetragen
- `Stadt` - Stadt für welche der Twitter Account zuständig ist, ist ein Account nur für ein Bundesland tätig (aber nicht für eine spezielle Stadt wird die Landeshauptstadtverwendet)
- `LAT` - Latitude (Breitengrad)
- `LONG` - Longitude (Längengrad)

## Hinweise
Es gibt bundesweite Accounts, die keinen Eintrag in Bundesland/Stadt/LAT/LONG haben. Diese Felder sind mit einem `-` Zeichen vermerkt.
Nur die ersten beiden Reihen der CSV Tabelle besitzen Einträge mit dem `-` Zeichen.

Die Daten wurden teils automatisch erzeugt und wurden nur teilweise überprüft. Das bedeutet, dass die Daten dadurch potentiell fehlerhaft oder nicht vollständig sein können. Das betrifft vor allem die Spalte `Typ`, vereinzelt können auch GPS-Koordinaten nicht zu den Orten passen, allerdings sollte der Großteil davon korrekt sein.

## Skript
Mit dem Python Skript: `gen_gps_map.py` könnt ihr eine HTML-Datei erstellen, die die GPS Koordinaten der Accounts grafisch auf einer Karte darstellt.
