# CopBird - Docs

## Grundlagen
- Prinzipiell würden wir euch empfehlen mit einem auf Linux-basiertem Betriebssystem wie z.B. Ubuntu zu arbeiten oder auch mit macOS.
- Mit Versionierungstools wie "Git" ist es möglich, kooperativ Code zu schreiben und diesen an zentraler Stelle hochzuladen. Git gibt es für Linux, MacOS und Windows.
Bezüglich der Wahl der Programmiersprache empfehlen wir euch 'Python' (https://www.python.org/) oder 'R' zu verwenden.
- In Jupyter Notebooks ist es möglich, sich das Ergebnis eines Programmierschritts wie Graphen oder Auswertungen direkt anzeigen zu lassen.
- Entwicklungsumgebungen nehmen am Anfang viel Arbeit ab und vereinfachen vieles.

Die Quick Start-Guides findet ihr hier:
- [Python](./docs/python.md)
- [Git](./docs/git.md)
- [Jupyter Notebook](./docs/jupyter.md)
- [Entwicklungsumgebungen](./docs/ide.md)

## Datenanalyse
Wenn ihr die Grundlagen bereits kennt, könnt ihr direkt mit der Exploration der Daten anfangen.
Um euch den Einstieg in die Daten zu erleichtern, haben wir euch verschiedene Anleitungen vorbereitet und zusätzliche Daten generiert:

- [Hands-On](./handson)
- [Graphanalyse mit PyVis](./graphanalysis)
- [Scrapen zusätzlicher Tweets mit `nitter-scraper` und `snscrape`](./scraping)
- [Topic Modelling](./topicmodelling)
- [Sentiment Analysis mit BERT](./sentiments)
- [Geografische Positionen der Twitter-Accounts](./geolocations)

## Ordnerstruktur
Sobald ihr die Daten heruntergeladen habt, legt die Daten in den Ordner `data` und überprüft, ob die Namen der CSV-Dateien übereinstimmen.

```
├── data/
|    ├── copbird_table_tweet.csv
|    ├── copbird_table_entity.csv
|    ├── copbird_table_user.csv
├── docs/
├── geolocations/
├── graphanalysis/
├── handson/
├── scraping/
├── sentiments/
├── topicmodelling/
```

## Erstellung von Videos

Für die Erstellung von Videos haben wir euch [hier](./docs/videos.md) ein paar Werkzeuge zusammengestellt, die ihr für den Screencast verwenden könnt.

## Quick Start (lokal)
1. Überprüfen, ob Python3 installiert ist.
```
python3 --version
```
Falls nicht, Python installieren:
- [Anleitung für Windows](https://installpython3.com/windows/)
- [Anleitung für Linux](https://installpython3.com/linux/)
- [Anleitung für macos](https://installpython3.com/mac/)

2. Überprüfen, ob `python3-pip` installiert ist. Falls nicht, Pip installieren: [Anleitung](https://pip.pypa.io/en/stable/installing/)
```
pip3 --version
```

3. `virtualenv` installieren.
```
pip3 install virtualenv
```

4. In den Projektordner wechseln und eine neue virtuelle Umgebung erstellen:
```
virtualenv copbird-venv
```
5. In die virtuelle Umgebung wechseln.
- Linux: `source copbird-venv/bin/activate`
- Windows: `copbird-venv/Scripts/activate.bat`

Um später die virtuelle Umgebung wieder zu verlassen:
- Linux: `deactivate`
- Windows: `copbird-venv/Scripts/deactivate.bat`

6. Abhängigkeiten installieren.
```
pip3 install -r requirements.txt
```

Sollte es zu Problemen mit den Abhängigkeiten kommen versucht bitte folgenden Befehl auszführen:
```
pip3 install -r requirements_fix.txt
```
7. Die neueste Version von `snscrape` installieren.
```
pip3 install git+https://github.com/JustAnotherArchivist/snscrape.git
```
8. spaCy-Modell laden.
```
python -m spacy download de_core_news_lg
```
9. Die CSV-Dateien aus dem Datensatz im Ordner `data` ablegen.
