from multiprocessing import Pool

import matplotlib.pyplot as plt
import pandas as pd
import pyLDAvis.sklearn
import spacy
from sklearn.decomposition import LatentDirichletAllocation
from sklearn.feature_extraction.text import CountVectorizer

NUM_TOPICS = 10
NUM_FEATURES = 1000
NUM_TOP_WORDS = 25

tweet_csv = '../data/copbird_table_tweet.csv'


def clean_tweet(doc):
    token_list = []

    for token in doc:
        if not token.is_alpha or token._.is_emoji or token.like_url or token.like_email or token.is_stop:
            pass
        else:
            token_list.append(token.lemma_)
    return ' '.join(token_list)


def get_tweets(path, limit=None):
    df_csv = pd.read_csv(path, nrows=limit, parse_dates=['created_at'],
                         encoding='utf-8-sig')

    df_csv.drop(columns=['created_at', 'like_count', 'retweet_count', 'reply_count', 'quote_count'], inplace=True)

    nlp = spacy.load("de_core_news_lg")
    nlp.Defaults.stop_words |= {"&amp", "amp"}
    nlp.add_pipe('emoji', first=True)
    return list(
        nlp.pipe(df_csv['tweet_text'], disable=["tok2vec", "tagger", "parser", "attribute_ruler"], n_process=-1))


def get_cleaned(twts):
    with Pool() as p:
        cleaned_twts = p.map_async(clean_tweet, twts).get()
    return cleaned_twts


def plot_top_words(model, feature_names, n_top_words, title):
    fig, axes = plt.subplots(2, 5, figsize=(30, 15), sharex=True)
    axes = axes.flatten()
    for topic_idx, topic in enumerate(model.components_):
        top_features_ind = topic.argsort()[:-n_top_words - 1:-1]
        top_features = [feature_names[i] for i in top_features_ind]
        weights = topic[top_features_ind]

        ax = axes[topic_idx]
        ax.barh(top_features, weights, height=0.7)
        ax.set_title(f'Topic {topic_idx + 1}',
                     fontdict={'fontsize': 30})
        ax.invert_yaxis()
        ax.tick_params(axis='both', which='major', labelsize=20)
        for i in 'top right left'.split():
            ax.spines[i].set_visible(False)
        fig.suptitle(title, fontsize=40)

    plt.subplots_adjust(top=0.90, bottom=0.05, wspace=0.90, hspace=0.3)
    fig.savefig('results/topics.svg', bbox_inches='tight')
    plt.show()


def selected_topics(model, vectorizer, top_n=10):
    for idx, topic in enumerate(model.components_):
        print("Topic %d:" % idx)
        print([(vectorizer.get_feature_names()[i], topic[i]) for i in topic.argsort()[:-top_n - 1:-1]])


if __name__ == '__main__':
    tweets = get_tweets(tweet_csv)
    cleaned = get_cleaned(tweets)

    count_vectorizer = CountVectorizer(min_df=5, max_df=0.9)
    tweets_vectorized = count_vectorizer.fit_transform(cleaned)

    lda = LatentDirichletAllocation(n_components=NUM_TOPICS, max_iter=10, learning_method='online', n_jobs=-1,
                                    verbose=True)
    lda.fit(tweets_vectorized)

    selected_topics(lda, count_vectorizer)
    lda_feature_names = count_vectorizer.get_feature_names()
    plot_top_words(lda, lda_feature_names, NUM_TOP_WORDS, 'TOPICS')
    dash = pyLDAvis.sklearn.prepare(lda, tweets_vectorized, count_vectorizer, mds='tsne')
    with open('results/LDA.html', 'w') as file:
        pyLDAvis.save_html(dash, file)
