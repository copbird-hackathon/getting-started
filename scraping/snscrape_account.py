import snscrape.modules.twitter as sntwitter
import csv

# python multiline comment
# contains arbitrary list of police twitter account names
usernames = """polizei_h
polizei_hal
polizeihamburg
pp_stuttgart
sh_polizei"""

# creates a file with name 'account_info.csv' that is automatically closed afterwards
with open("account_info.csv", "w") as f:
    # use '\t' as CSV delimiter since the data contains ',' characters
    writer = csv.writer(f, delimiter="\t")

    print("username\tdisplayname\tlocation\tfollowersCount\tfriendsCount\tmediaCount")

    # split up list of account names that we want to query
    for name in usernames.split("\n"):
        # only request one tweet from the account (we only need the account information once)
        tweet = next(sntwitter.TwitterUserScraper(name).get_items())

        # use user-information from tweet
        # (user class definition: https://github.com/JustAnotherArchivist/snscrape/blob/master/snscrape/modules/twitter.py#L112)
        user = tweet.user

        # print some simple information to stdout
        print(
            f"{user.username}\t{user.displayname}\t{user.location}\t{user.followersCount}\t{user.friendsCount}\t{user.mediaCount}"
        )

        # save username, displayname and account location to the CSV file
        writer.writerow((user.username, user.displayname, user.location))
