# import needed snscrape module and pandas
import snscrape.modules.twitter as sntwitter
import pandas as pd

# for test purposes only scrape 10 tweets
maxTweets = 10
# create an empty list to store the retrieved tweets
tweets_list = []

for i, tweet in enumerate(
    # scrape all tweets that contain the word 'Demo' and are replying to the account 'polizeiberlin'
    # add the output of all these tweets to the tweets_list
    sntwitter.TwitterSearchScraper("Demo (to:polizeiberlin)").get_items()
    # scrape all tweets from the 'polizeiberlin' account that have more than 7 likes
    # sntwitter.TwitterSearchScraper("(from:polizeiberlin) min_faves:7").get_items()
    # scrape all tweets from the 'polizeiberlin' account that have more than 7 likes
    # sntwitter.TwitterSearchScraper("(from:polizeiberlin) min_faves:7").get_items()
    # scrape all tweets that contain the hashtag: 'AbolishThePolice'
    # sntwitter.TwitterSearchScraper("(from:polizeiberlin)").get_items()
):
    if i > maxTweets:
        break
    tweets_list.append(
        [
            tweet.id,
            tweet.user,
            tweet.date,
            tweet.content,
            tweet.likeCount,
            tweet.outlinks,
            tweet.tcooutlinks,
            tweet.url,
        ]
    )

# Creating a pandas dataframe from the tweets list above and assign the coloumns with specific names
# pandas DataFrames are a powerful tool to represent data in a tabular way
# these DataFrames consist of coloumns and rows which both can be sorted and rearranged
tweets_df = pd.DataFrame(
    tweets_list,
    columns=[
        "ID",
        "User",
        "Date",
        "Content",
        "likeCount",
        "Outlinks",
        "TCOoutlinks",
        "URL",
    ],
)

# print the first 10 rows
print(tweets_df.head(10))

# only print the 'Content' coloumn of the first 10 rows
print(tweets_df["Content"].head(10))
